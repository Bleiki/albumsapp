//
//  AlbumInfoViewController.swift
//  AlbumsApp
//
//  Created by Vista on 09.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class AlbumInfoViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var primaryGenreNameLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var trackCountLabel: UILabel!
    @IBOutlet weak var collectionPriceLabel: UILabel!
    
    @IBOutlet weak var collectionNameTF: UITextField!
    @IBOutlet weak var artistNameTF: UITextField!
    @IBOutlet weak var primaryGenreNameTF: UITextField!
    @IBOutlet weak var releaseDateTF: UITextField!
    @IBOutlet weak var trackCountTF: UITextField!
    @IBOutlet weak var collectionPriceTF: UITextField!
    
    //MARK: - Properties
    var album: Album!
    var albumImage: UIImage?

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.settingSubviews()
    }
    
    //MARK: - SupportMethods
    fileprivate func settingSubviews() {
        
        let havePrice = self.album.collectionPrice != nil && self.album.currency != nil
        
        if let image = self.albumImage {
            self.imageView.image = image 
        } else {
            self.imageView.image = UIImage(named: "iTunes-100")
            self.getAlbumImage()
        }
        
        self.artistNameLabel.text       = "Artist name"
        self.trackCountLabel.text       = "Track count"
        self.releaseDateLabel.text      = "Release date"
        self.collectionNameLabel.text   = "Album name"
        self.collectionPriceLabel.text  = "Album price"
        self.primaryGenreNameLabel.text = "Primary genre name"
        
        self.artistNameTF.text       = self.album.artistName
        self.trackCountTF.text       = self.album.trackCount != nil ? "\(self.album.trackCount!)" : "Not specified"
        self.releaseDateTF.text      = self.getCorrectDate()
        self.collectionNameTF.text   = self.album.collectionName
        self.collectionPriceTF.text  = havePrice ? "\(self.album.collectionPrice!) \(self.album.currency!)" : "Not specified"
        self.primaryGenreNameTF.text = self.album.primaryGenreName
    }
    
    fileprivate func getCorrectDate() -> String {
        
        if let data = self.album.releaseDate {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            
            return dateFormatter.string(from: data)
        }
        
        return "Not specified"
    }
    
    fileprivate func getAlbumImage() {
        
        if let url = self.album.artworkUrl100 {
            
            AlbumsDataManager.instance.getImageAt(url: url, success: { (image) in
                
                if image != nil {
                    self.imageView.image = image
                }
            })
        }
    }
}
