//
//  ViewController.swift
//  AlbumsApp
//
//  Created by Vista on 06.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: - IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    let albumInfoVCId = "AlbumInfoViewController"
    let cellsInRow    = 2 //Number cells in row.
    var albums        = [Album]()
    
    let cellsInset: CGFloat = 10.0 //Insets for cells.
    
    var activityIndicator: UIActivityIndicatorView?
    var messageLabel: UILabel? //Message for empty albums.
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingSubviews()
    }
    
    //MARK: - UICollectionViewDataSource
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.albums.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "AlbumCell"
        let album      = self.albums[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! AlbumsCollectionViewCell
        
        cell.artistNameLabel.text = album.artistName
        cell.albumNameLabel.text  = album.collectionName
        cell.imageView.image      = UIImage(named: "iTunes-100")
        
        weak var weakCell = cell
        
        self.getImageFor(album: album, in: weakCell)
        
        return cell
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let edge = (self.view.bounds.width - self.cellsInset * CGFloat((self.cellsInRow + 1))) / CGFloat(self.cellsInRow)
        let size = CGSize(width: edge, height: edge)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: self.cellsInset, left: self.cellsInset, bottom: self.cellsInset, right: self.cellsInset)
    }
    
    //MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let albumInfoVC = self.storyboard?.instantiateViewController(withIdentifier: self.albumInfoVCId) as! AlbumInfoViewController
        albumInfoVC.album = self.albums[indexPath.row]
        
        if let albumCell = self.collectionView.cellForItem(at: indexPath) as? AlbumsCollectionViewCell {
            albumInfoVC.albumImage = albumCell.imageView.image
        }
        
        self.navigationController?.pushViewController(albumInfoVC, animated: true)
    }
    
    //MARK: - SupportMethods
    fileprivate func settingSubviews() {
        
        self.navigationItem.title  = "Albums"
        self.searchBar.placeholder = "Search..."
        
        //Set color for placeholder text.
        if let textField = self.searchBar.value(forKey: "searchField") as? UITextField {
            if let label = textField.value(forKey: "placeholderLabel") as? UILabel {
                
                label.textColor = .black
            }
            
            //Set color for search image.
            if let iconView = textField.leftView as? UIImageView {
                
                iconView.image     = iconView.image?.withRenderingMode(.alwaysTemplate)
                iconView.tintColor = .black
            }
        }
        
        //Set color for cancel btn.
        for view in self.searchBar.subviews[0].subviews {
            if let btn = view as? UIButton {
                
                btn.setTitleColor(.black, for: .normal)
            }
        }
    }
    
    func search() {
        
        if let text = self.searchBar.text, !text.characters.isEmpty {
            
            self.createAndRunActivityIndicator()
            
            AlbumsDataManager.instance.searchMusicAlbumsAt(text: text, success: { (albums) in
                
                self.albums = albums.sorted(by: { return ($0.collectionName ?? "") < ($1.collectionName ?? "")}) //Sorting in alphabetical order.
                
                self.stopActivityIndicator()
                self.checkAlbums() //Check count of albums.
                
                self.collectionView.reloadData() //Reload data at finish search.
                
            }) { (error) in
                print(error ?? "Can't get albums!!!")
            }
        }
    }
    
    func getImageFor(album: Album, in cell: UICollectionViewCell?) {
        
        if let url = album.artworkUrl100 {
            
            AlbumsDataManager.instance.getImageAt(url: url, success: { (image) in
                
                if let someCell = cell as? AlbumsCollectionViewCell {
                    if image != nil {
                        someCell.imageView.image = image //Set album image at finish launching.
                    }
                }
            })
        }
    }
}

