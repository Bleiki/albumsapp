//
//  SearchVCExtension.swift
//  AlbumsApp
//
//  Created by Vista on 09.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController: UISearchBarDelegate {
    
    //MARK: - UISearchBarDelegate
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.search()
    }
}

extension SearchViewController {
    
    //MARK: - SuppportMethods
    func createAndRunActivityIndicator() {
        
        if self.activityIndicator == nil {
            
            let actIndView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            
            actIndView.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
            actIndView.startAnimating()
            
            self.view.addSubview(actIndView)
            self.view.bringSubview(toFront: actIndView)
            
            self.activityIndicator = actIndView
            
        } else {
            
            self.activityIndicator?.startAnimating()
            self.view.bringSubview(toFront: self.activityIndicator!)
        }
    }
    
    func stopActivityIndicator() {
        
        if self.activityIndicator != nil {
            
            self.view.sendSubview(toBack: self.activityIndicator!)
            self.activityIndicator?.stopAnimating()
        }
    }
    
    func checkAlbums() {
        
        if self.albums.isEmpty {
            
            if self.messageLabel == nil {
                
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 300.0, height: 30.0))
                
                label.textAlignment = .center
                label.center        = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
                label.text          = "Albums not found!"
                
                self.view.addSubview(label)
                self.view.bringSubview(toFront: label)
                
                self.messageLabel = label
                
            } else {
                self.view.bringSubview(toFront: self.messageLabel!)
            }
            
        } else {
            
            if self.messageLabel != nil {
                self.view.sendSubview(toBack: self.messageLabel!)
            }
        }
    }
}

