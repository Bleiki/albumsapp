//
//  AlbumsCollectionViewCell.swift
//  AlbumsApp
//
//  Created by Vista on 09.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class AlbumsCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
}
