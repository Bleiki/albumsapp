//
//  Album.swift
//  AlbumsApp
//
//  Created by Vista on 06.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import Foundation

class Album {
    
    //MARK: - Properties
    var currency: String?
    var artistName: String?
    var trackCount: Int?
    var releaseDate: Date?
    var artworkUrl100: String?
    var collectionName: String?
    var collectionPrice: Float?
    var primaryGenreName: String?
    
    //MARK: LifeCycle
    init(dataResponse: [String : Any]) {

        self.currency         = dataResponse["currency"] as? String
        self.artistName       = dataResponse["artistName"] as? String
        self.trackCount       = dataResponse["trackCount"] as? Int
        self.artworkUrl100    = dataResponse["artworkUrl100"] as? String
        self.collectionName   = dataResponse["collectionName"] as? String
        self.collectionPrice  = dataResponse["collectionPrice"] as? Float
        self.primaryGenreName = dataResponse["primaryGenreName"] as? String
        
        if let date = dataResponse["releaseDate"] as? String {
            self.releaseDate = Album.getCorrectDateFrom(string: date)
        }
    }
    
    //MARK: - SupportMethods
    fileprivate class func getCorrectDateFrom(string: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let components    = string.components(separatedBy: "T") //Division at date and time.
        let correctString = components.first //Get date component.
        
        let date = dateFormatter.date(from: correctString ?? "")
        
        return date
    }
}

