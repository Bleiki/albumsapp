//
//  AlbumsDataManager.swift
//  AlbumsApp
//
//  Created by Vista on 09.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import Foundation
import Alamofire

class AlbumsDataManager {
    
    //MARK: - Properties
    static let instance = AlbumsDataManager()
    
    let baseURL = "https://itunes.apple.com"
    let successSttusCode = 200
    
    //MARK: - ServerMethods
    func searchMusicAlbumsAt(text: String, success: @escaping (_ albums: [Album]) -> (), failure: @escaping (_ error: Error?) -> ()) {
        
        let url        = self.baseURL + "/search"
        var parameters = [String : Any]()
        
        parameters["term"]   = text
        parameters["media"]  = "music" //Search in music.
        parameters["limit"]  = 190     //Max number albums in response.
        parameters["entity"] = "album" //From music get albums.
        
        Alamofire.request(
            url,
            method: .get,
            parameters: parameters,
            encoding: URLEncoding.default,
            headers: nil).responseJSON { (dataResponse) in
                
                if dataResponse.response?.statusCode == self.successSttusCode {
                    if let result = dataResponse.result.value as? [String : Any], let arrayOfParams = result["results"] as? [[String : Any]] {
                        
                        var albums = [Album]()
                        
                        for params in arrayOfParams {
                            let album = Album(dataResponse: params)
                            albums.append(album)
                        }
                        
                        success(albums)
                        
                    } else {
                        failure(dataResponse.error)
                    }
                } else {
                    failure(dataResponse.error)
                }
        }
    }
    
    func getImageAt(url: String, success: @escaping (_ image: UIImage?) -> ()) {
        
        Alamofire.request(url).responseData { (dataResponse) in
            
            if let data = dataResponse.data {
                
                let image = UIImage(data: data)
                success(image)
                
            } else {
                print("Can't get image at URL: \(url)")
            }
        }
    }
}
